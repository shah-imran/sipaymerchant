<nav class="page-sidebar custom-page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="pt-2 pb-2 pl-3 pr-3 flexbox res-btn-show" style="background-color: #ffffff" href="">
            <a href="index.html">
                <img src="assets/img/merchant_imgs/logo.png" height="50" width="auto" />
            </a>
        </div>
        <div class="pt-2 pb-2 pl-3 pr-3 flexbox res-btn-show" style="background-color: #f0f0f0" href="">
            <input type="text" placeholder="Saerch" style="width: 80%; background-color: #f0f0f0;" class="mr-2 border-0" />
            <i class="fa fa-search text-muted"></i>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="main-menu" href="javascript:;">
                    <img src="assets/img/merchant_imgs/sidebar/transactions.svg" />
                    <span class="nav-label">TRANSACTIONS</span>
                    <i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="">Transactions</a>
                    </li>
                    <li>
                        <a href="">Withdrawals</a>
                    </li>
                    <li>
                        <a href="">Cashouts</a>
                    </li>
                    <li>
                        <a href="">Refunds</a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="nav-label">Reporting</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-3-level collapse">
                            <li>
                                <a href="">Reconciliation</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a class="main-menu" href="javascript:;">
                    <img src="assets/img/merchant_imgs/sidebar/dpl.svg" />
                    <span class="nav-label">CREATING DPL</span>
                </a>
            </li>
            <li>
                <a class="main-menu" href="javascript:;">
                    <img src="assets/img/merchant_imgs/sidebar/withdraw.svg" />
                    <span class="nav-label">WITHDRAW</span>
                    <i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="">Withdraw</a>
                    </li>
                    <li>
                        <a href="">Early Withdraw</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="main-menu" href="javascript:;">
                    <img src="assets/img/merchant_imgs/sidebar/cashout.svg" />
                    <span class="nav-label">CASH OUT</span>
                    <i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="">API</a>
                    </li>
                    <li>
                        <a href="">Upload List</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="main-menu" href="javascript:;">
                    <img src="assets/img/merchant_imgs/sidebar/b2b.svg" />
                    <span class="nav-label">B2B PAYMENT</span>
                </a>
            </li>
            <li>
                <a class="main-menu" href="javascript:;">
                    <img src="assets/img/merchant_imgs/sidebar/settings.svg" />
                    <span class="nav-label">SETTINGS</span>
                    <i class="fa fa-angle-left arrow"></i>
                </a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="">API</a>
                    </li>
                    <li>
                        <a href="">User Management</a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="nav-label">Payment Option</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-3-level collapse">
                            <li>
                                <a href="">Wallet</a>
                            </li>
                            <li>
                                <a href="">Cards</a>
                            </li>
                            <li>
                                <a href="">MCB</a>
                            </li>
                            <li>
                                <a href="">Instant Loan</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a class="main-menu" href="javascript:;">
                    <img src="assets/img/merchant_imgs/sidebar/campaign.svg" />
                    <span class="nav-label">CAMPAIGN</span>
                </a>
            </li>
            <li>
                <a class="main-menu" href="javascript:;">
                    <img src="assets/img/merchant_imgs/sidebar/support.svg" />
                    <span class="nav-label">SUPPORT</span>
                </a>
            </li>
        </ul>

    </div>
</nav>
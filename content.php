<div style="padding: 0 15px;">
    <div class="row">
        <div class="col-lg-4 col-md-12 p-0">
            <span style="color: #6820d5;" class="">Main</span>
            <span class="text-muted ml-2 mr-2">/</span>
            <span class="text-muted">Transaction</span>
        </div>
        <div class="col-lg-8 col-md-12 p-0 text-lg-right">
            <span class="text-muted mr-4">Total Balance<strong class="text-dark ml-2">1,000 TL</strong></span>
            <span class="text-muted">Available Balance<strong class="text-dark ml-2">1,000 TL</strong></span>
        </div>
    </div>
</div>
<hr>

<div class="page-content fade-in-up pt-0">
    <span class="d-block text-muted mb-2 mt-4">Excutive summery</span>
    <div style="padding: 0 15px;" class="pb-4 mb-2">
        <div class="row" style="background: linear-gradient(to right, #2155f7, #8c57f0);">
            <div class="p-4 col-lg-3 col-md-6 text-center text-white" style="border-bottom: 4px solid #6720d4;">
                Today
                <h4 class="m-0 p-0 pt-2 pb-1">9</h4>
                <small>Transactions</small>
            </div>
            <div class="p-4 col-lg-3 col-md-6 text-center text-white" style="border-bottom: 4px solid #03a9f5;">
                Last Week
                <h4 class="m-0 p-0 pt-2 pb-1">123</h4>
                <small>Transactions</small>
            </div>
            <div class="p-4 col-lg-3 col-md-6 text-center text-white" style="border-bottom: 4px solid #6720d4;">
                Last Month
                <h4 class="m-0 p-0 pt-2 pb-1">123456</h4>
                <small>Transactions</small>
            </div>
            <div class="p-4 col-lg-3 col-md-6 text-center text-white" style="border-bottom: 4px solid #03a9f5;">
                Last Year
                <h4 class="m-0 p-0 pt-2 pb-1">123456789</h4>
                <small>Transactions</small>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col-sm-1">
                    <label class="mt-2 sidebar-item-icon">Date</label>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <div class="input-group-icon input-group-icon-right">
                            <span class="input-icon input-icon-right">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control" type="text" placeholder="24.06.2019">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <div class="input-group-icon input-group-icon-right">
                            <span class="input-icon input-icon-right">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control" type="text" placeholder="29.06.2019">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-flex">
                        <label class="mt-2 sidebar-item-icon mr-3">Currency</label>
                        <button class="btn white-btn text-muted mr-1">
                            TL &nbsp;
                            <i class="fa fa-turkish-lira"></i>
                        </button>
                        <button class="btn white-btn text-muted mr-1">
                            EUR &nbsp;
                            <i class="fa fa-euro"></i>
                        </button>
                        <button class="btn white-btn text-muted">
                            USD &nbsp;
                            <i class="fa fa-dollar"></i>
                        </button>
                    </div>
                </div>

            </div>
        </div>

<!--        <div class="col-sm-1">-->
<!--            <label class="mt-2 sidebar-item-icon">Currency</label>-->
<!--        </div>-->
<!--        <div class="col-sm-1">-->
<!--            <button class="btn white-btn">-->
<!--                TL &nbsp;-->
<!--                <i class="fa fa-turkish-lira"></i>-->
<!--            </button>-->
<!--        </div>-->
<!--        <div class="col-sm-1">-->
<!--            <button class="btn white-btn">-->
<!--                EUR &nbsp;-->
<!--                <i class="fa fa-euro"></i>-->
<!--            </button>-->
<!--        </div>-->
<!--        <div class="col-sm-1">-->
<!--            <button class="btn white-btn">-->
<!--                USD &nbsp;-->
<!--                <i class="fa fa-dollar"></i>-->
<!--            </button>-->
<!--        </div>-->



    </div>

<!--    <div class="row">-->
<!--        <div class="col-7">-->
<!--            <div class="row">-->
<!--                <div class="col-1">-->
<!--                    <label class="mt-2 sidebar-item-icon">Date</label>-->
<!--                </div>-->
<!--                <div class="col-5">-->
<!--                    <div class="form-group">-->
<!--                        <div class="input-group-icon input-group-icon-right">-->
<!--                            <span class="input-icon input-icon-right">-->
<!--                                <i class="fa fa-calendar"></i>-->
<!--                            </span>-->
<!--                            <input class="form-control" type="text" placeholder="24.06.2019">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-5 custom-row">-->
<!--                    <div class="form-group">-->
<!--                        <div class="input-group-icon input-group-icon-right">-->
<!--                            <span class="input-icon input-icon-right">-->
<!--                                <i class="fa fa-calendar"></i>-->
<!--                            </span>-->
<!--                            <input class="form-control" type="text" placeholder="29.06.2019">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-5">-->
<!--            <div class="row">-->
<!--                <div class="col-3">-->
<!--                    <label class="mt-2 sidebar-item-icon">Currency</label>-->
<!--                </div>-->
<!--                <div class="col-3">-->
<!--                    <button class="btn white-btn">-->
<!--                        TL &nbsp;-->
<!--                        <i class="fa fa-turkish-lira"></i>-->
<!--                    </button>-->
<!--                </div>-->
<!--                <div class="col-3">-->
<!--                    <button class="btn white-btn">-->
<!--                        EUR &nbsp;-->
<!--                        <i class="fa fa-euro"></i>-->
<!--                    </button>-->
<!--                </div>-->
<!--                <div class="col-3">-->
<!--                    <button class="btn white-btn">-->
<!--                        USD &nbsp;-->
<!--                        <i class="fa fa-dollar"></i>-->
<!--                    </button>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

<br>
    <div class="row">
        <div class="col-sm-7">
            <div class="ibox">
                <div class="ibox-body">
                    <div class="d-flex justify-content-between mb-4">
                        <div>
                            <h3 class="m-0">Completed transaction</h3>
                            <div class="text-muted">Overview of Latest Transactions</div>
                        </div>
                    </div>
                    <hr/>
                    <div>
                        <canvas id="visitors_chart_c" style="height:260px;"></canvas>
                    </div>
                </div>
                <hr>
                <div class="ibox-body">

                    <div class="row">
                        <div class="col-3">
                            <h6 class="mb-3">Today</h6>
                            <p>03</p>
                        </div>
                        <div class="col-3">
                            <h6 class="mb-3">Last Week</h6>
                            <p>123</p>
                        </div>
                        <div class="col-3">
                            <h6 class="mb-3">Last Month</h6>
                            <p>1,234</p>
                        </div>
                        <div class="col-3">
                            <h6 class="mb-3">Last Year</h6>
                            <p>12,234</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-5">
            <div class="ibox ibox-fullheight">
                <div class="ibox-body">
                    <h3 class="m-0">Refunds</h3>
                    <div class="text-muted">Overview of Latest Refunds</div>
                </div>
                <hr/>
                <div class="ibox-body">
                    <p class="text-center m-0">
                        <input class="dial" value="75%" data-width="110" data-height="110" data-thickness=".2"
                               data-fgcolor="#1654FC" data-max="100" readonly data-displayprevious="true" type="text"/>
                    </p>

                </div>

                <div class="ibox-body pb-0">
                        <h5 class="h5 m-0">Completed Refunds</h5>
                </div>

                <div class="ibox-body">

                    <div class="row">
                        <div class="col-6">
                            <p><strong>12</strong> completed</p>
                        </div>
                        <div class="col-6">
                            <p class="pull-right">70%</p>
                        </div>
                    </div>
                    <div class="progress mb-4">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                             aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                             style="width: 20%;height: 10px"></div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p><strong>3</strong> progressing</p>
                        </div>
                        <div class="col-6">
                            <p class="pull-right">26%</p>
                        </div>
                    </div>
                    <div class="progress mb-4">
                        <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar"
                             aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"
                             style="width: 10%;height: 10px;background-color: #E24942"></div>
                    </div>
                </div>


            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Stats of Transaction</div>
                </div>
                <div class="ibox-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="mr-2" />All</th>
                                <th>Transaction ID</th>
                                <th>Status</th>
                                <th>Date & Time</th>
                                <th>Payment Method</th>
                                <th>Amount</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>123456789</td>
                                <td class="text-success">Approved</td>
                                <td>01-05-2019 - 12.00</td>
                                <td>Credit Card</td>
                                <td>40,00 TL</td>
                                <td><button class="btn btn-sm text-dark">Show Details</button></td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>123456789</td>
                                <td class="text-primary">Refunded</td>
                                <td>01-05-2019 - 12.00</td>
                                <td>Credit Card</td>
                                <td>40,00 TL</td>
                                <td><button class="btn btn-sm text-dark">Show Details</button></td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>123456789</td>
                                <td class="text-danger">Declined</td>
                                <td>01-05-2019 - 12.00</td>
                                <td>Credit Card</td>
                                <td>40,00 TL</td>
                                <td><button class="btn btn-sm text-dark">Show Details</button></td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>123456789</td>
                                <td class="text-success">Approved</td>
                                <td>01-05-2019 - 12.00</td>
                                <td>Credit Card</td>
                                <td>40,00 TL</td>
                                <td><button class="btn btn-sm text-dark">Show Details</button></td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>123456789</td>
                                <td class="text-primary">Refunded</td>
                                <td>01-05-2019 - 12.00</td>
                                <td>Credit Card</td>
                                <td>40,00 TL</td>
                                <td><button class="btn btn-sm text-dark">Show Details</button></td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>123456789</td>
                                <td class="text-danger">Declined</td>
                                <td>01-05-2019 - 12.00</td>
                                <td>Credit Card</td>
                                <td>40,00 TL</td>
                                <td><button class="btn btn-sm text-dark">Show Details</button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
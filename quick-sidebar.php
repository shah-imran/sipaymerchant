<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<div class="quick-sidebar">
    <div class="pt-4 mt-4">
        <a class="d-block text-center p-4 ml-4 mr-4" href="">
            <img class="img-circle" src="./assets/img/merchant_imgs/users/u7.jpg" alt="image" width="100" />
            <strong class="d-block font-16 pt-3 text-dark">Merchant Name</strong>
            <span class="text-dark">Istanbul, TR</span>            
        </a>
        <div class="border border-muted border-top-0 border-left-0 border-right-0 p-4">
            <div class="pl-4 pr-4">
                <label>Upload Photo</label>
                <a class="p-3 rounded flexbox" style="background-color: #f0f0f0" href="">
                    <span class="text-secondary">Choose file</span>
                    <i class="fa fa-file text-muted"></i>
                </a>
            </div>
        </div>
        <div class="p-4 ml-4 mr-4 flexbox text-center" id="pro-link">
            <a class="" href="">
                <img src="assets/img/merchant_imgs/settings.svg" width="50" />
                <strong class="d-block text-dark">Settings</strong>
            </a>
            <a class="" href="">
                <img src="assets/img/merchant_imgs/support.svg" width="50" />
                <strong class="d-block text-dark">Support</strong>
            </a>
            <a class="" href="">
                <i class="fa fa-sign-out p-0 m-0" style="font-size: 50px; color: #000000"></i>
                <strong class="d-block text-dark">Sign out</strong>
            </a>
        </div>
    </div>    
</div>
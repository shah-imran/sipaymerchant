<header class="header">
    <div class="container flexbox flex-1">
        <div class="res-btn-hide">
            <a href="index.html">
                <img src="assets/img/merchant_imgs/logo.png" height="50" width="auto" />
            </a>
        </div>
        <div class="flexbox flex-1">
            <!-- START TOP-LEFT TOOLBAR-->
            <ul class="nav navbar-toolbar">
                <li class="res-btn-show">
                    <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </li>
                <li class="res-btn-hide">
                    <div class="pt-2 pb-2 pl-3 pr-3 rounded flexbox" style="background-color: #f0f0f0" href="">
                        <input type="text" placeholder="Saerch" style="width: 200px; background-color: #f0f0f0;" class="mr-3 border-0" />
                        <i class="fa fa-search text-muted"></i>
                    </div>
                </li>
            </ul>
            <!-- END TOP-LEFT TOOLBAR-->
            <!-- START TOP-RIGHT TOOLBAR-->
            <ul class="nav navbar-toolbar">
                <li class="dropdown dropdown-inbox">
                    <a class="nav-link dropdown-toggle toolbar-icon" data-toggle="dropdown" href="javascript:;"><i class="ti-bell rel"></i>
                        <span class="envelope-badge">7</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                        <div class="dropdown-arrow"></div>
                        <div class="dropdown-header text-center">
                            <div>
                                <span class="text-muted"><strong>7 New</strong> Notifications</span>
                            </div>
                        </div>
                        <div class="p-3">
                            <div class="media-list media-list-divider scroller" data-height="350px" data-color="#71808f">
                                <a class="media p-3">
                                    <div class="media-img">
                                        <img class="img-circle" src="./assets/img/merchant_imgs/users/u7.jpg" alt="image" />
                                    </div>
                                    <div class="media-body">
                                        <div class="media-heading">Lynn Weaver</div>
                                        <div class="font-13 text-muted">Your proposal interested me.</div>
                                    </div>
                                </a>
                                <a class="media p-3">
                                    <div class="media-img">
                                        <img class="img-circle" src="./assets/img/merchant_imgs/users/u6.jpg" alt="image" />
                                    </div>
                                    <div class="media-body">
                                        <div class="font-strong">Connor Perez</div>
                                        <div class="font-13 text-muted">Your proposal interested me.</div>
                                    </div>
                                </a>
                                <a class="media p-3">
                                    <div class="media-img">
                                        <img class="img-circle" src="./assets/img/merchant_imgs/users/u11.jpg" alt="image" />
                                    </div>
                                    <div class="media-body">
                                        <div class="font-strong">Tyrone Carroll</div>
                                        <div class="font-13 text-muted">Your proposal interested me.</div>
                                    </div>
                                </a>
                                <a class="media p-3">
                                    <div class="media-img">
                                        <img class="img-circle" src="./assets/img/merchant_imgs/users/u10.jpg" alt="image" />
                                    </div>
                                    <div class="media-body">
                                        <div class="font-strong">Stella Obrien</div>
                                        <div class="font-13 text-muted">Your proposal interested me.</div>
                                    </div>
                                </a>
                                <a class="media p-3">
                                    <div class="media-img">
                                        <img class="img-circle" src="./assets/img/merchant_imgs/users/u2.jpg" alt="image" />
                                    </div>
                                    <div class="media-body">
                                        <div class="font-strong">Becky Brooks</div>
                                        <div class="font-13 text-muted">Your proposal interested me.</div>
                                    </div>
                                </a>
                                <a class="media p-3">
                                    <div class="media-img">
                                        <img class="img-circle" src="./assets/img/merchant_imgs/users/u5.jpg" alt="image" />
                                    </div>
                                    <div class="media-body">
                                        <div class="font-strong">Bob Gonzalez</div>
                                        <div class="font-13 text-muted">Your proposal interested me.</div>
                                    </div>
                                </a>
                                <a class="media p-3">
                                    <div class="media-img">
                                        <img class="img-circle" src="./assets/img/merchant_imgs/users/u9.jpg" alt="image" />
                                    </div>
                                    <div class="media-body">
                                        <div class="font-strong">Tammy Newman</div>
                                        <div class="font-13 text-muted">Your proposal interested me.</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="dropdown-footer text-center pt-3 pb-3 border border-muted">
                            <a href="" class="text-primary">View All Notifications</a>
                        </div>
                    </div>
                </li>
                <li class="dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle link quick-sidebar-toggler font-14">
                        <span>Merchant Name</span>
                        <img src="./assets/img/merchant_imgs/users/u7.jpg" alt="image" />
                    </a>
                </li>
                <li class="dropdown">
                    <a class="nav-link dropdown-toggle toolbar-icon2 font-14" data-toggle="dropdown" href="javascript:;">
                        <span class="pr-2">ENG</span><i class="fa fa-caret-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                        <div class="dropdown-arrow"></div>
                        <div class="p-3">
                            <a class="d-block p-3">
                                <img src="assets/img/merchant_imgs/uk.png" alt="image" />
                                <span>ENG</span>
                            </a>
                            <a class="d-block p-3">
                                <img src="assets/img/merchant_imgs/tr.png" alt="image" />
                                <span class="d-inline-flex align-items-center text-primary">TUR</span>
                            </a>
                        </div>                       
                    </div>
                </li>
                <li>
                    <a class="nav-link link font-14">
                        <i class="fa fa-sign-out p-0 m-0" style="font-size: 25px; color: #000000; opacity: 0.25;"></i>
                    </a>
                </li>
            </ul>
            <!-- END TOP-RIGHT TOOLBAR-->
        </div>
    </div>
</header>

<script>
    $(function () {
        var ctx = document.getElementById("visitors_chart_c").getContext("2d");
        var visitors_chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                datasets: [
                    {
                        label: "",
                        data: [300, 100, 200, 300, 200, 120, 400, 500],
                        borderColor: 'rgba(117,54,230,0.9)',
                        backgroundColor: 'rgba(117,54,230,0.9)',
                        pointBackgroundColor: 'rgba(117,54,230,0.9)',
                        pointBorderColor: 'rgba(117,54,230,0.9)',
                        borderWidth: 1,
                        pointBorderWidth: 1,
                        pointRadius: 0,
                        pointHitRadius: 30,
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                showScale: false,
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                            drawBorder: false
                        }
                    }]
                },
                legend: {
                    labels: false
                },
            }
        });

    });

</script>
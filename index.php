<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Adminca bootstrap 4 &amp; angular 5 admin template, Шаблон админки | Dashboard</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="./assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="./assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="./assets/vendors/line-awesome/css/line-awesome.min.css" rel="stylesheet"/>
    <link href="./assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet"/>
    <link href="./assets/vendors/animate.css/animate.min.css" rel="stylesheet"/>
    <link href="./assets/vendors/toastr/toastr.min.css" rel="stylesheet"/>
    <link href="./assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet"/>
    <!-- PLUGINS STYLES-->
    <link href="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
    <!-- THEME STYLES-->
    <link href="assets/css/main.min.css" rel="stylesheet"/>
    <link href="assets/css/sipaymerchanttemplate.css" rel="stylesheet"/>
    <!-- PAGE LEVEL STYLES-->
</head>
<body class="fixed-navbar">
<div class="page-wrapper">
    <!-- START HEADER-->
    <?php include 'header.php'; ?>
    <!-- END HEADER-->

    <div class="container pt-4 mt-4">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <?php include 'sidebar.php'; ?>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="content-wrapper content-wrapper-custom">
                    <!-- START PAGE CONTENT-->
                    <?php include 'content.php'; ?>
                    <!-- END PAGE CONTENT-->

                </div>
            </div>

        </div>

        <hr class="custom-hr"/>
        <footer class="page-footer">
            <div class="font-13">2019 © <b style="color: #03a9f5">Sipay</b>. All right reserved</div>
            <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
        </footer>
    </div>


</div>
<!-- START SEARCH PANEL-->
<form class="search-top-bar" action="search.html">
    <input class="form-control search-input" type="text" placeholder="Search...">
    <button class="reset input-search-icon"><i class="ti-search"></i></button>
    <button class="reset input-search-close" type="button"><i class="ti-close"></i></button>
</form>
<!-- END SEARCH PANEL-->
<!-- BEGIN PAGA BACKDROPS-->
<div class="sidenav-backdrop backdrop"></div>
<div class="preloader-backdrop">
    <div class="page-preloader">Loading</div>
</div>
<!-- END PAGA BACKDROPS-->
<!-- New question dialog-->
<div class="modal fade" id="session-dialog">
    <div class="modal-dialog" style="width:400px;" role="document">
        <div class="modal-content timeout-modal">
            <div class="modal-body">
                <button class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="text-center mt-3 mb-4"><i class="ti-lock timeout-icon"></i></div>
                <div class="text-center h4 mb-3">Set Auto Logout</div>
                <p class="text-center mb-4">You are about to be signed out due to inactivity.<br>Select after how many
                    minutes of inactivity you log out of the system.</p>
                <div id="timeout-reset-box" style="display:none;">
                    <div class="form-group text-center">
                        <button class="btn btn-danger btn-fix btn-air" id="timeout-reset">Deactivate</button>
                    </div>
                </div>
                <div id="timeout-activate-box">
                    <form id="timeout-form" action="javascript:;">
                        <div class="form-group pl-3 pr-3 mb-4">
                            <input class="form-control form-control-line" type="text" name="timeout_count"
                                   placeholder="Minutes" id="timeout-count">
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-primary btn-fix btn-air" id="timeout-activate">Activate</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End New question dialog-->
<!-- QUICK SIDEBAR-->
<?php include 'quick-sidebar.php'; ?>
<!-- END QUICK SIDEBAR-->
<!-- CORE PLUGINS-->
<script src="./assets/vendors/jquery/dist/jquery.min.js"></script>
<script src="./assets/vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="./assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="./assets/vendors/metisMenu/dist/metisMenu.min.js"></script>
<script src="./assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="./assets/vendors/jquery-idletimer/dist/idle-timer.min.js"></script>
<script src="./assets/vendors/toastr/toastr.min.js"></script>
<script src="./assets/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="./assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<!-- PAGE LEVEL PLUGINS-->
<script src="./assets/vendors/chart.js/dist/Chart.min.js"></script>
<script src="./assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="./assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
<script src="./assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="./assets/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- CORE SCRIPTS-->
<script src="assets/js/app.min.js"></script>
<!-- PAGE LEVEL SCRIPTS-->
<script src="./assets/js/scripts/dashboard_visitors.js"></script>


<?php include 'chartjs.php'?>

<script>
    // jQuery Knob
    $(".dial").knob({
        'format' : function (value) {
            return value + '%';
        }
    });
</script>

</body>

</html>